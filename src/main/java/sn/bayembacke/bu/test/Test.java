package sn.bayembacke.bu.test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import sn.bayembacke.bu.be.*;
import sn.bayembacke.bu.service.IDictionnaire;
import sn.bayembacke.bu.service.IDocument;
import sn.bayembacke.bu.service.ILivre;
import sn.bayembacke.bu.service.IRevue;


import java.util.*;

@SpringBootApplication(scanBasePackages={"sn.bayembacke.bu"})
public class Test {
    private static Set<Integer> generatedNumbers = new HashSet<>();
    public static int generateRandomNumber() {
        int randomNumber;
        do {
            Calendar now = Calendar.getInstance();
            int year = now.get(Calendar.YEAR);
            int month = now.get(Calendar.MONTH) + 1;
            int increment = new Random().nextInt(1000) + 1;
            randomNumber = Integer.parseInt(Integer.toString(year) + String.format("%02d", month) + String.format("%03d", increment));
        } while (generatedNumbers.contains(randomNumber));

        generatedNumbers.add(randomNumber);
        return randomNumber;
    }

        public static void main(String[] args) {
            ApplicationContext context = SpringApplication.run(Test.class, args);
            IDocument documentDao =context.getBean(IDocument.class);
            IDictionnaire dictionary = context.getBean(IDictionnaire.class);
            ILivre daoLivre = context.getBean(ILivre.class);
            IRevue daoRevue = context.getBean(IRevue.class);

            Dictionnaire dictionnaire = new Dictionnaire(
                    generateRandomNumber(),
                    "AS Salih",
                    "Français"
            );
            System.out.println("================================================================");
            System.out.println("================================================================");
            dictionary.addDictionnaire(dictionnaire);
            List<Dictionnaire> dictionnaire1 = dictionary.getDictionnaire(dictionnaire.getLangue());
            for (Dictionnaire d : dictionnaire1){
                System.out.println(d.getLangue());
            }

            System.out.println("================================================================");
            System.out.println("================================================================");

            List<Document> allDocuments = documentDao.getDocumentList();

            for (Document doc : allDocuments){
                System.out.println(doc.getNumEnreg());
            }
            System.out.println("================================================================");
            System.out.println("================================================================");

            List<Dictionnaire> dic = dictionary.getDictionnaireList();
            for (Dictionnaire doc : dic){
                System.out.println(doc.getNumEnreg());
            }

            System.out.println("================================================================");
            System.out.println("================================================================");

            Livre livre = new Livre(
                    generateRandomNumber(),
                    "AS Salih",
                    "Mor Surang",
                    200,
                    TypeLivre.ROMAN
            );
            daoLivre.addLivre(livre);
            List<Livre> mesLivres = daoLivre.getLivreList();

            for (Livre livres : mesLivres) {
                List<Livre> auteurLivres = daoLivre.getLivre(livres.getAuteur());
                System.out.println(livres.getNbrPages() + " " + livres.getTypeLivre());
                System.out.println("================================================================");
                System.out.println("================================================================");

                for (Livre liv: auteurLivres) {
                    System.out.println(liv.getNbrPages() + " " + liv.getTypeLivre());
                }

            }
            System.out.println("================================================================");
            System.out.println("================================================================");

            Revue revue = new Revue(
                    generateRandomNumber(),
                    "Chambre 7",
                    11,
                    2024
            );
            daoRevue.addRevue(revue);
            System.out.println("================================================================");
            System.out.println("================================================================");

            List<Revue> mesRevues = daoRevue.getRevuesList();
            for (Revue revue1: mesRevues) {
                System.out.println("List================================================================");
                System.out.println("List================================================================");
                System.out.println(revue1.getNumEnreg() + " " + revue1.getAnnee());
                List<Revue> mesRevuesParAnnee = daoRevue.getRevue(revue1.getAnnee());
                for (Revue revue2: mesRevuesParAnnee){
                    System.out.println("Annee================================================================");
                    System.out.println("Annee================================================================");
                    System.out.println(revue1.getNumEnreg() + " " + revue1.getAnnee());
                    System.out.println("================================================================");
                    System.out.println("================================================================");
                }
                List<Revue> mesRevuesParMois = daoRevue.getRevue(revue1.getMois());
                for (Revue revue3: mesRevuesParMois){
                    System.out.println("Mois================================================================");
                    System.out.println("Mois================================================================");
                    System.out.println(revue3.getNumEnreg() + " " + revue3.getMois());
                    System.out.println("================================================================");
                    System.out.println("================================================================");
                }

            }

             }


}
