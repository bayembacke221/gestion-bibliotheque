package sn.bayembacke.bu.service;

import sn.bayembacke.bu.be.Livre;

import java.util.List;

public interface ILivre {
    Livre addLivre(Livre livre);
    List<Livre> getLivreList();
    List<Livre> getLivre(String auteur);
    Livre updateLivre(Livre livre);
    void deleteLivre(Long id);
}
