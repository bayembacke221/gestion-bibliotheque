package sn.bayembacke.bu.service;

import sn.bayembacke.bu.be.Document;

import java.util.*;

public interface IBibliotheque {

    List<Document> mesDocuments();

    public static Set<Integer> generatedNumbers = new HashSet<>();
    public static int generateRandomNumber() {
        int randomNumber;
        do {
            Calendar now = Calendar.getInstance();
            int year = now.get(Calendar.YEAR);
            int month = now.get(Calendar.MONTH) + 1;
            int increment = new Random().nextInt(1000) + 1;
            randomNumber = Integer.parseInt(Integer.toString(year) + String.format("%02d", month) + String.format("%03d", increment));
        } while (generatedNumbers.contains(randomNumber));

        generatedNumbers.add(randomNumber);
        return randomNumber;
    }
}
