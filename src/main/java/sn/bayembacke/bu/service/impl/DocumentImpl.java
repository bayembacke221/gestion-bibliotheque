package sn.bayembacke.bu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.bayembacke.bu.be.Document;
import sn.bayembacke.bu.dao.DocumentDao;
import sn.bayembacke.bu.service.IDocument;

import java.util.List;
@Service
public class DocumentImpl implements IDocument {


    @Autowired
    DocumentDao documentDao;
    @Override
    public List<Document> getDocumentList() {
        return documentDao.findAll();
    }

    @Override
    public Document getDocumentById(int id) {
        return documentDao.findDocumentByNumEnreg(id);
    }
}
