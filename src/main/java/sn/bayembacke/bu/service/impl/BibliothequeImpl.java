package sn.bayembacke.bu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.bayembacke.bu.be.Document;
import sn.bayembacke.bu.dao.DocumentDao;
import sn.bayembacke.bu.service.IBibliotheque;

import java.util.List;
@Service
public class BibliothequeImpl implements IBibliotheque {

    @Autowired
    DocumentDao documentDao;
    @Override
    public List<Document> mesDocuments() {
        return documentDao.findAll();
    }
}
