package sn.bayembacke.bu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.bayembacke.bu.be.Dictionnaire;
import sn.bayembacke.bu.be.Document;
import sn.bayembacke.bu.dao.DictionnaireDao;
import sn.bayembacke.bu.service.IBibliotheque;
import sn.bayembacke.bu.service.IDictionnaire;

import java.util.List;
@Service
public class DictionnaireImpl  implements IDictionnaire {
    @Autowired
    DictionnaireDao dao;
    @Override
    public Dictionnaire addDictionnaire(Dictionnaire dictionary) {
        dictionary.setNumEnreg(IBibliotheque.generateRandomNumber());
        return dao.save(dictionary);
    }

    @Override
    public List<Dictionnaire> getDictionnaireList() {
        return dao.findAll();
    }

    @Override
    public  List<Dictionnaire> getDictionnaire(String langue) {
        return dao.findDictionnaireByLangue(langue);
    }

    @Override
    public Dictionnaire updateDictionnaire(Dictionnaire dictionary) {
        if (dictionary != null){
            dictionary.setNumEnreg(IBibliotheque.generateRandomNumber());
            return dao.save(dictionary);
        }
        return null;
    }

    @Override
    public void deleteDictionnaire(Long id) {

        if (dao.findById(id) != null){

            dao.deleteById(id);

        }
    }
}
