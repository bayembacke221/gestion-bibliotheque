package sn.bayembacke.bu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.bayembacke.bu.be.Revue;
import sn.bayembacke.bu.dao.RevueDao;
import sn.bayembacke.bu.service.IBibliotheque;
import sn.bayembacke.bu.service.IRevue;

import java.time.LocalDate;

import java.util.List;
@Service
public class RevueImpl implements IRevue {
    @Autowired
    RevueDao revueDao;
    @Override
    public Revue addRevue(Revue revue) {
        if (revue.getMois() >= 1 && revue.getMois()  <= 12 && revue.getAnnee() >= LocalDate.now().getYear()) {
            revue.setNumEnreg(IBibliotheque.generateRandomNumber());
            return revueDao.save(revue);
        } else {
            System.out.println("La date est invalide !");
            return null;
        }
    }

    @Override
    public List<Revue> getRevuesList() {
        return revueDao.findAll();
    }

    @Override
    public List<Revue> getRevue(int date) {
        return revueDao.findRevueByAnneeAndMois(date);
    }

    @Override
    public Revue updateRevue(Revue revue) {
        if (revue==null) return null;
        revue.setNumEnreg(IBibliotheque.generateRandomNumber());
        return revueDao.save(revue);
    }

    @Override
    public void deleteRevue(Long id) {
        if (revueDao.findById(id) != null) {
            revueDao.deleteById(id);
        }
    }

}
