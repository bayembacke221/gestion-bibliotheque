package sn.bayembacke.bu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.bayembacke.bu.be.Livre;
import sn.bayembacke.bu.dao.LivreDao;
import sn.bayembacke.bu.service.IBibliotheque;
import sn.bayembacke.bu.service.ILivre;

import java.util.List;
@Service
public class LivreImpl implements ILivre {
    @Autowired
    LivreDao livreDao;
    @Override
    public Livre addLivre(Livre livre) {
        livre.setNumEnreg(IBibliotheque.generateRandomNumber());
        return livreDao.save(livre);
    }

    @Override
    public List<Livre> getLivreList() {
        return livreDao.findAll();
    }

    @Override
    public List<Livre>  getLivre(String auteur) {
        return livreDao.findLivreByAuteur(auteur);
    }

    @Override
    public Livre updateLivre(Livre livre) {
        if (livre == null) {
            return null;
        }
        livre.setNumEnreg(IBibliotheque.generateRandomNumber());
        return livreDao.save(livre);
    }

    @Override
    public void deleteLivre(Long id) {
        if (livreDao.findById(id)!=null) {
            livreDao.deleteById(id);
        }
    }
}
