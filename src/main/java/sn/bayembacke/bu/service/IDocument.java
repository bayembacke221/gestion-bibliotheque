package sn.bayembacke.bu.service;

import sn.bayembacke.bu.be.Document;

import java.util.Calendar;
import java.util.List;

public interface IDocument {
    List<Document> getDocumentList();
    Document getDocumentById(int id);

}
