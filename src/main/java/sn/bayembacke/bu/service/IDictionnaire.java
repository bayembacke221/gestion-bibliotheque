package sn.bayembacke.bu.service;

import sn.bayembacke.bu.be.Dictionnaire;
import sn.bayembacke.bu.be.Document;

import java.util.List;

public interface IDictionnaire {
    Dictionnaire addDictionnaire(Dictionnaire dictionary);
    List<Dictionnaire> getDictionnaireList();
    List<Dictionnaire> getDictionnaire(String langue);
    Dictionnaire updateDictionnaire(Dictionnaire dictionary);
    void deleteDictionnaire(Long id);
}
