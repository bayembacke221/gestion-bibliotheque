package sn.bayembacke.bu.service;

import sn.bayembacke.bu.be.Revue;

import java.util.List;

public interface IRevue {
    Revue addRevue(Revue revue);
    List<Revue> getRevuesList();
    List<Revue> getRevue(int date);
    Revue updateRevue(Revue revue);
    void deleteRevue(Long id);
}
