package sn.bayembacke.bu.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.bayembacke.bu.be.Revue;
import sn.bayembacke.bu.service.IRevue;

import java.util.List;

@RestController
@RequestMapping("/api/v1/revue")
public class RevueController {

    @Autowired
    IRevue revueService;


    @GetMapping
    public ResponseEntity<List<Revue>> getRevues(){

        return new ResponseEntity<>(revueService.getRevuesList(), HttpStatus.OK);
    }
    @GetMapping("/date/{date}")
    public ResponseEntity<List<Revue>> getRevuesByDate(@PathVariable("date") int date) {

        return new ResponseEntity<>(revueService.getRevue(date), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Revue> addRevue(@RequestBody
                                          Revue revue) {
        return new ResponseEntity<>(revueService.addRevue(revue), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Revue> updateRevue(@RequestBody Revue revue) {
        return new ResponseEntity<>(revueService.updateRevue(revue), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity deleteRevue(@PathVariable ("id") Long id) {
        revueService.deleteRevue(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
