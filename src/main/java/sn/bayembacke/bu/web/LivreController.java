package sn.bayembacke.bu.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.bayembacke.bu.be.Livre;
import sn.bayembacke.bu.service.ILivre;

import java.util.List;

@RestController
@RequestMapping("/api/v1/livre")
public class LivreController {

    @Autowired
    ILivre livreService;


    @GetMapping
    public ResponseEntity<List<Livre>> getAllLivres() {

        return new ResponseEntity<>(livreService.getLivreList(), HttpStatus.OK);
    }
    @GetMapping("/author/{author}")
    public ResponseEntity<List<Livre>> getLivresByAuthor(@PathVariable ("author") String author){
        return new ResponseEntity<>(livreService.getLivre(author), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Livre> addLivre(@RequestBody Livre livre){

        return new ResponseEntity<>(livreService.addLivre(livre), HttpStatus.CREATED);

    }
    @PutMapping
    public ResponseEntity<Livre> updateLivre(@RequestBody Livre livre){

        return new ResponseEntity<>(livreService.updateLivre(livre), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity deleteLivre(@PathVariable("id") Long id){
        livreService.deleteLivre(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
