package sn.bayembacke.bu.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.bayembacke.bu.be.Dictionnaire;
import sn.bayembacke.bu.service.IDictionnaire;

import java.util.List;

@RestController
    @RequestMapping("/api/v1/dictionnaire")
public class DictionnaireController {

    @Autowired
    IDictionnaire dictionnaireService;

    @GetMapping
    public ResponseEntity<List<Dictionnaire>> getAllDictionaries() {
        return new ResponseEntity<>(dictionnaireService.getDictionnaireList(), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Dictionnaire> addDictionnaire(@RequestBody Dictionnaire dictionnaire){

        return new
                ResponseEntity<>(dictionnaireService.addDictionnaire(dictionnaire),
                HttpStatus.CREATED);
    }
    @GetMapping("/language/{language}")
    public ResponseEntity<List<Dictionnaire>> getDictionnaireByLanguage(
            @PathVariable("language") String language){

        return new ResponseEntity<>(dictionnaireService.getDictionnaire(language),HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Dictionnaire> updateDictionnaire(@RequestBody Dictionnaire dictionary){

        return new ResponseEntity<>(dictionnaireService.updateDictionnaire(dictionary),HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity deleteDictionnaire(@PathVariable("id") Long id){
        dictionnaireService.deleteDictionnaire(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
