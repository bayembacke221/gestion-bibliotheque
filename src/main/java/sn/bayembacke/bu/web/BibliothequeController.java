package sn.bayembacke.bu.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.bayembacke.bu.be.Document;
import org.springframework.http.HttpStatus;
import sn.bayembacke.bu.service.IBibliotheque;

import java.util.List;

@RestController
@RequestMapping("/api/v1/bu")
public class BibliothequeController {

    @Autowired
    IBibliotheque bibliotheque;

    @GetMapping
    public ResponseEntity<List<Document>> getDocument(){

        return new ResponseEntity<>(bibliotheque.mesDocuments(),HttpStatus.OK);

    }
}
