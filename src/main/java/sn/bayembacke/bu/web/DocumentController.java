package sn.bayembacke.bu.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.bayembacke.bu.be.Document;
import sn.bayembacke.bu.service.IDocument;

import java.util.List;

@RestController
@RequestMapping("/api/v1/document")
public class DocumentController {
    @Autowired
    IDocument documentDao;

    @GetMapping
    public ResponseEntity<List<Document>> getDocument(){

        return new ResponseEntity<>(documentDao.getDocumentList(), HttpStatus.OK);

    }
}
