package sn.bayembacke.bu.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.bayembacke.bu.be.Dictionnaire;

import java.util.List;

@Repository
public interface DictionnaireDao extends JpaRepository<Dictionnaire,Long> {
    List<Dictionnaire> findDictionnaireByLangue(String langue);
}
