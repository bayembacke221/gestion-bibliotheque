package sn.bayembacke.bu.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.bayembacke.bu.be.Livre;

import java.util.List;

@Repository
public interface LivreDao extends JpaRepository<Livre,Long> {
    List<Livre> findLivreByAuteur(String auteur);
}
