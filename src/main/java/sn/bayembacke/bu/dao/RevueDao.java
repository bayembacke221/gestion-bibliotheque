package sn.bayembacke.bu.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sn.bayembacke.bu.be.Revue;

import java.util.List;

@Repository
public interface RevueDao extends JpaRepository<Revue,Long> {
    @Query("SELECT r FROM Revue r WHERE cast(r.mois as string)" +
            " LIKE %?1% or cast(r.annee as string) LIKE %?1%")
    List<Revue> findRevueByAnneeAndMois(int date);
}
