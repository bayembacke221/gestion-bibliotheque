package sn.bayembacke.bu.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.bayembacke.bu.be.Document;
@Repository
public interface DocumentDao extends JpaRepository<Document,Long> {
    Document findDocumentByNumEnreg(int numEnreg);
}
