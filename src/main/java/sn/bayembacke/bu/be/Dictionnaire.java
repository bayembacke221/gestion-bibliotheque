package sn.bayembacke.bu.be;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "dictionnaires")
@DiscriminatorValue("DICTIONNAIRE")
public class Dictionnaire extends Document implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String langue;

    public Dictionnaire() {
    }

    public Dictionnaire(int numEnreg, String titre, String langue) {
        super(numEnreg, titre);
        this.langue = langue;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }
}
