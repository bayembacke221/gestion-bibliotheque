package sn.bayembacke.bu.be;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "revues")
public class Revue extends Document implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int mois;
    private int annee;

    public Revue() {
    }

    public Revue(int numEnreg, String titre, int mois, int annee) {
        super(numEnreg, titre);
        this.mois = mois;
        this.annee = annee;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public int getMois() {
        return mois;
    }

    public void setMois(int mois) {
        this.mois = mois;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }
}
