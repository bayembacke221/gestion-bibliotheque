package sn.bayembacke.bu.be;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "livres")
public class Livre  extends Document implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String auteur;
    private int nbrPages;
    @Enumerated(EnumType.STRING)
    private TypeLivre typeLivre;

    public Livre() {
    }

    public Livre(int numEnreg, String titre, String auteur, int nbrPages, TypeLivre typeLivre) {
        super(numEnreg, titre);
        this.auteur = auteur;
        this.nbrPages = nbrPages;
        this.typeLivre = typeLivre;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public int getNbrPages() {
        return nbrPages;
    }

    public void setNbrPages(int nbrPages) {
        this.nbrPages = nbrPages;
    }

    public TypeLivre getTypeLivre() {
        return typeLivre;
    }

    public void setTypeLivre(TypeLivre typeLivre) {
        this.typeLivre = typeLivre;
    }
}
